import Client from 'objects/Client';
import RainbowText from 'objects/RainbowText';

/* ES6 Module */
import testModule from 'Modules/myModule';
import { add, subtract } from 'Modules/Math';
import * as math from 'Modules/Math'; //import all module as math object
import devide from 'Modules/Math'; //Here -devide- is default function of Math module.
//import devide, {add, subtract} from 'Modules/Math'; // We can use this pattern to load other function with default.

class Main extends Phaser.State {
	constructor(){
		super();
	    this.client = new Client;
	}

    create(){
    	console.log('Main.js-GameState');

    	/* ES6 Module Example */
	    	testModule('Neat'); //Default export
	    	const total = add(2,4);
	    	const subTotal = subtract(6,2);
	    	console.log(`add result: ${total}`); //named export function
	    	console.log(`subtract result: ${subTotal}`); //name export variable

	    	//Import all the module as an object
	    	console.log(`divide default function from math module: ${devide(10, 2)}`);
	    	console.log(`divide default function from math module: ${math.default(10, 2)}`);
	    	console.log(`add function from math module: ${math.add(2, 4)}`);
	    	console.log(`subtract method from math module: ${math.subtract(6, 2)}`);
	    	console.log(`Name variable from math module: ${math.name}`);
    	/* End ES6 Module Example */

    	this.client.sendTest();

        this.playerMap = {};
        this.map = this.add.tilemap('map');
        this.map.addTilesetImage('tilesheet', 'tileset'); // tilesheet is the key of the tileset in map's JSON file
        this.layer;
        for(var i = 0; i < this.map.layers.length; i++) {
            this.layer = this.map.createLayer(i);
        }

        this.layer.inputEnabled = true; // Allows clicking on the map ; it's enough to do it on the last layer
	    this.layer.events.onInputUp.add(this.getCoordinates, this);
	    this.client.askNewPlayer();

	    this.client.io.on('newplayer', (data) =>{
	    	this.addNewPlayer(data.id,data.x,data.y);
		});

		this.client.io.on('allplayers', (data) =>{
			for(var i = 0; i < data.length; i++){
				this.addNewPlayer(data[i].id, data[i].x, data[i].y);
		    }

		    this.client.io.on('move', (data) =>{
		        this.movePlayer(data.id,data.x,data.y);
		    });

		    this.client.io.on('remove', (id) =>{
		        this.removePlayer(id);
		    });
		});
    }


    getCoordinates(layer, pointer){
	    this.client.sendClick(pointer.worldX, pointer.worldY);
	}

	addNewPlayer(id, x, y){
	    this.playerMap[id] = this.add.sprite(x, y, 'sprite');
	}

	movePlayer(id, x, y){
	    let player = this.playerMap[id];
	    let distance = Phaser.Math.distance(player.x, player.y, x, y);
	    let tween = this.add.tween(player);
	    let duration = distance*10;
	    tween.to({x:x,y:y}, duration);
	    tween.start();
	}

	removePlayer(id){
	    this.playerMap[id].destroy();
	    delete this.playerMap[id];
	}
	
}
 
export default Main;